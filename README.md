# Processo Seletivo - Avaliação Técnica

Essa etapa visa avaliar qual o nível técnico dos candidatos, em relação à metodologia e nível de programação que utilizará para resolver um problema. É nessa etapa que o candidato deverá mostrar o que sabe fazer. 


# Problema

O problema está relacionado a uma marca que possui diversas franquias distribuídas pelo Brasil. 

1) O primeiro objetivo é avaliar qual região tem maior potencial, avaliando por meio de um índice que considera a quantidade de clientes ativos sobre a população da cidade. (Utilize a base de dados ‘demograficas’ para enriquecer a base de franquias)

2) O segundo objetivo é avaliar o crescimento médio anual mês a mês geral e de cada franquia, comparando 2019 e 2021.

# Desafio

Utilizando a base de dados “distancia”, avalie a distância média percorrida por cada vendedor por dia. 

# Entrega

Poderá ser entregue os códigos desenvolvidos para as análises, dashboards, relatórios e ou apresentações, lembrando que o objetivo dessa etapa é avaliar a capacidade de analisar dados e apresentar os resultados.
